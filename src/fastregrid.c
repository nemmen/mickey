#include "fastregrid.h"

#pragma acc routine seq
int search(double xref, int length, double *x) {
    /* 
    Returns the index corresponding to the element in array x with
    value nearest xref. Obtains the difference between the array
    elements and xref, and finds the minimum.
    
    Inspired on https://codereview.stackexchange.com/a/5146/161148
    */
    int i, minindex;
    double diff;
    double minimum;

    // starting values for search
    minimum = fabs(x[0]-xref); //diff[0];
    minindex=0;

    // minimum search
    for (i = 1; i < length; ++i) {
    	diff=fabs(x[i]-xref);
        if (minimum > diff) {
            minimum = diff;
            minindex=i;
        }
    }

    return minindex;
}







void regrid(int nxnew, double *xnew, int nynew, double *ynew, int n1, double *r, int n2, double *th, double *rho, double *p, double *v1, double *v2, double *v3, double *rhonew, double *pnew, double *vx, double *vy, double *vz) {
	/*
	Performs change of coordinate basis and regridding of arrays from a
	polar to cartesian basis.

	Variables t* are temporary ones which are not required in the code, only
	for the purposes of using numpy.
	*/

	int i,j;
	int nnew, iref, jref, nref;
	double rnew,thnew;

	// goes through new array
    #pragma acc data copyout(rhonew[0:nxnew*nynew], pnew[0:nxnew*nynew], vx[0:nxnew*nynew], vy[0:nxnew*nynew], vz[0:nxnew*nynew]) copyin(xnew[0:nxnew],ynew[0:nynew],r[0:n1],th[0:n2],rho[0:n1*n2],p[0:n1*n2],v1[0:n1*n2],v2[0:n1*n2],v3[0:n1*n2])  
    {
	#pragma acc parallel loop collapse(2)
	for (i=0; i<nxnew; i++) {
		for (j=0; j<nynew; j++) {
  			// Need to use 1D index for accessing array elements 
			nnew=i*nynew+j;

			// generates new polar coordinates arrays
			rnew=sqrt(xnew[i]*xnew[i] + ynew[j]*ynew[j]); // new r
			thnew=atan2(ynew[j], xnew[i]);	// new theta

			// locates position in old coordinate arrays
			iref=search(rnew,n1,r);
			jref=search(thnew,n2,th);
			nref=iref*n2+jref;	// 1d index

			// assigns arrays in new coord. basis
			rhonew[nnew]=rho[nref];
			pnew[nnew]=p[nref];

			// cartesian components of velocity vector 
			vx[nnew]=v1[nref]*cos(th[jref])-v2[nref]*sin(th[jref]);
			vy[nnew]=v1[nref]*sin(th[jref])+v2[nref]*cos(th[jref]);			
			vz[nnew]=v3[nref]; // vphi
		}	
	} // end acc kernels
	} // end acc data region
}

